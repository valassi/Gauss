// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:55 GMT
#ifndef MINUIT_PARAMETER_SET
#define MINUIT_PARAMETER_SET

#include <vector>
#include <iostream>
#include "TMinuit.h"

#include "AmpGen/MinuitParameter.h"
#include "AmpGen/ParsedParameterFile.h"
#include "AmpGen/ParsedParameterFileList.h"
class TNtupleD;
class TFile;
namespace AmpGen{

  class MinuitParameterSet{
    protected:
      std::vector<MinuitParameter*> _parPtrList;
      static const char ntpNameChars[];
      static const char prtNameChars[];
      static std::string prtToNtpName(const std::string& s_in);

      bool addToEnd(MinuitParameter* parPtr);
      bool setAllIndices();
    public:

      MinuitParameterSet();
      MinuitParameterSet(const MinuitParameterSet& other);

      MinuitParameterSet getFloating();

      bool add(MinuitParameter* parPtr);
      bool unregister(MinuitParameter* patPtr);

      unsigned int size()const;

      //  double getVal(int i) const;
      MinuitParameter* getParPtr(unsigned int i);
      const MinuitParameter* getParPtr(unsigned int i) const;
      
      MinuitParameter* getParPtr( const std::string& key ) const;
      
      std::vector<MinuitParameter*>::const_iterator cbegin() const { return _parPtrList.cbegin() ; }
      std::vector<MinuitParameter*>::const_iterator   cend() const { return _parPtrList.cend() ; }
      
      std::vector<MinuitParameter*>::iterator begin() { return _parPtrList.begin() ; }
      std::vector<MinuitParameter*>::iterator   end() { return _parPtrList.end() ; }


      // regarding the two routines below:
      // normally you don't want to use deleteListAndObjects()
      // This object is a collection of pointers
      // but it does not own the objects being
      // pointed to. However, if you created
      // the objects pointed to with 'new', this might
      // be a useful routine for memory management. Otherwise
      // use deleteListKeepObjects(). Or nothing at all.

      void deleteListAndObjects(); 
      // pitfall: delete objects in use
      void deleteListKeepObjects();
      // pitfall: potential memory leak

      TNtupleD* makeNewNtpForOwner(TFile*& ntpFile) const;
      std::string ntpNames() const;
      void fillNtp(TFile*& ntpFile, TNtupleD*& ntp) const;

      void print(std::ostream& os = std::cout) const;
      void printVariable(std::ostream& os = std::cout) const;
      void printResultVsInput(std::ostream& os = std::cout) const;

  };

}//namespace AmpGen
#endif
//
