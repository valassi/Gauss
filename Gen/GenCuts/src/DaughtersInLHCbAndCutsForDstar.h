#ifndef GENCUTS_DAUGHTERSINLHCBANDCUTSFORDSTAR_H
#define GENCUTS_DAUGHTERSINLHCBANDCUTSFORDSTAR_H 1

// Include files
#include "DaughtersInLHCb.h"

/** @class DaughtersInLHCbAndCutsForDstar DaughtersInLHCbAndCutsForDstar.h
 *
 *  Tool to keep events with daughters from signal particles
 *  in LHCb and with p and pt cuts on Dstar daughters.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Patrick Robbe
 *  @date   2012-02-07
 */
class DaughtersInLHCbAndCutsForDstar : public DaughtersInLHCb, virtual public IGenCutTool {
 public:
  /// Standard constructor
  DaughtersInLHCbAndCutsForDstar( const std::string& type,
                                  const std::string& name,
                                  const IInterface* parent);

  virtual ~DaughtersInLHCbAndCutsForDstar( ); ///< Destructor

  /** Accept events with daughters in LHCb and p/pt cuts on Dstar daughters
   *  (defined by min and max angles, different values for charged and neutrals)
   *  Implements IGenCutTool::applyCut.
   */
  virtual bool applyCut( ParticleVector & theParticleVector ,
                         const HepMC::GenEvent * theEvent ,
                         const LHCb::GenCollision * theCollision ) const ;

 private:
  /** Study a particle a returns true when all stable daughters
   *  are in LHCb AndWithMinP
   */
  bool passCuts( const HepMC::GenParticle * theSignal ) const ;

  // cut value of D0 pt
  double m_d0ptCut ;

  // cut value on daughters min pt
  double m_daughtersptminCut ;

  // cut value on daughters max pt
  double m_daughtersptmaxCut ;

  // cut value on daughters min p
  double m_daughterspminCut ;

  // soft pion pt cut
  double m_softpiptCut ;

  // cut on D0 ctau
  double m_d0ctauCut ;
};
#endif // GENCUTS_DAUGHTERSINLHCDANDCUTSFORDSTAR_H
