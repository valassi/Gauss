#!/usr/bin/env sh
export NO_GIT_CONDDB=1
echo "Running RadLength Test Part 1/2"
python $SIMCHECKSROOT/scripts/rad_length_scan.py
echo "Running RadLength Test Part 2/2"
python $SIMCHECKSROOT/scripts/rad_length_scan_velo_z.py
